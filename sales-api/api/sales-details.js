const { getDateRange, initialStats } = require('../domain')

const getIndexNameFrom = salesRep => salesRep == 0 ? "AllEventIndex" : "SalesRepIdEventsIndex"

module.exports = (router, docClient) => {
    router.get("/sales-details", async (req, res, next) => {
        // Access the provided 'page' and 'limt' query parameters
        console.log(req.query);
        let period = req.query.period;
        let salesRep = req.query.salesRep;
        let today = new Date();
        let { from, to } = getDateRange(period, today)
        let indexName = getIndexNameFrom(salesRep)
        let pk_index_name = "SALES_REP_ID"
        let ExpressionAttributeNames = ""

        if (salesRep == 0) {
            salesRep = "ALL"// + today.getFullYear().toString()
            pk_index_name = "#all"

            ExpressionAttributeNames = {
                '#all': "ALL"
            }
        }
        let KeyConditionExpression = `${pk_index_name} = :pk and EVENT_DATE between :sk1 and :sk2`

        const params = {
            TableName: "SALES_EVENT",

            IndexName: indexName,
            "KeyConditionExpression": KeyConditionExpression,
            "ExpressionAttributeNames": ExpressionAttributeNames,
            "ExpressionAttributeValues": {
                ":pk": salesRep,
                ":sk1": from,
                ":sk2": to,
            },
            // "ProjectionExpression": "UserId, TopScore",
            "ScanIndexForward": false
        }
        console.log(params)
        let result = {
            sales_events: [],
            funnel_stats: initialStats()
        }
        docClient.query(params, function scanUntilDone(err, data) {
            if (err) {
                console.error(err);
                res.json({
                    success: false,

                    message: "Error: Server error"
                });
            } else {

                if (data.LastEvaluatedKey) {
                    console.log(`LastEvaluatedKey: ${data.LastEvaluatedKey}`)
                    params.ExclusiveStartKey = data.LastEvaluatedKey;
                    updateResults(result, data.Items)
                    dynamodb.scan(params, scanUntilDone);
                } else {
                    // all results processed. done
                    updateResults(result, data.Items)

                    res.json({
                        success: true,
                        message: "Loaded Sales Events",
                        ...result
                    });
                    console.log(result.sales_events.length)

                }
            }
        });
    })
}

const updateResults = (result, Items) => {
    console.log("Items", Items.length)
    if (Items.length > 0) {

        result.sales_events = result.sales_events.concat(Items)
        console.log("updateResults", result.sales_events.length)

        result.funnel_stats = getEngagementStatusCounts(Items, result.funnel_stats)
        console.log(result.funnel_stats)
    }

}

const getEngagementStatusCounts = (items, funnel_stats) => {

    return items.reduce((stats, item) => {

        // console.log(stats, item)
        if (item.engagementStatus in stats)
            stats[item.engagementStatus] += 1
        return stats
    }, funnel_stats)
}

