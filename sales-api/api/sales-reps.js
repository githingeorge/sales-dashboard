module.exports = (router, docClient) => {

    router.get("/sales-reps", (req, res, next) => {

        var params = {
            TableName: "SALES_REP",
            ProjectionExpression: "id, #name",
            ExpressionAttributeNames: {
                "#name": "name"
            }
        };

        docClient.scan(params, function (err, data) {
            if (err) {
                console.error(err);
                res.json({
                    success: false,
                    message: "Error: Server error"
                });
            } else {
                const { Items } = data;
                console.log(Items)
                res.json({
                    success: true,
                    message: "",
                    sales_reps: Items
                });
            }
        });
    });

}