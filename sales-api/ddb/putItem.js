const uuidv4 = require("uuid/v4");
const { engagementStatuses, getStatus, meetingLocation } = require('../domain')
// Load the AWS SDK for Node.js
var AWS = require("aws-sdk");
// Set the region
AWS.config.update({ region: "us-east-2" });

// Create the DynamoDB service object
var ddb = new AWS.DynamoDB({ apiVersion: "2012-08-10" });

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
var today = new Date();
var start_date = new Date(2019, 06, 01);
var thisWeek = new Date(
  new Date(today).setDate(today.getDate() - getRandomInt(0, 7))
);
var lastWeek = new Date(
  new Date(today).setDate(today.getDate() - getRandomInt(7, 14))
);
var lastMonth = new Date(
  new Date(today).setDate(today.getDate() - getRandomInt(7, 30))
);
const getEventDate = (date, inc = 1) => {
  date.setDate(date.getDate() + getRandomInt(0, inc))
  return date
}
const toTimestamp = date => date.getTime().toString()
const formatedDate = date => `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;

var event_date = ""
var params = {
  RequestItems: {
    SALES_EVENT: [
      {
        PutRequest: {
          // sales rep 1 Lead In
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: '1' },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(start_date, 0)) },
            salesRepName: { S: "Jane Doe" },
            client: { S: "Accenture" },
            primaryContactPerson: { S: "Dew Dilbey" },
            title: { S: "C Level" },
            vertical: { S: "Senior Management" },
            engagementStatus: { S: engagementStatuses.leadIn },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }
          },
        },
      },
      {
        PutRequest: {
          // sales rep 1 Contact Made
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: '1' },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: new Date(2019, 06, 16).getTime().toString() },
            salesRepName: { S: "Jane Doe" },
            client: { S: "Accenture" },
            primaryContactPerson: { S: "Dew Dilbey" },
            title: { S: "C Level" },
            vertical: { S: "Senior Management" },
            meetingLocation: { S: meetingLocation.call },
            purposeOfInteraction: { S: "Negotiations" },
            outcomeAndRemarks: {
              S:
                "He needs to get final approval from the board. Will happen by today EOD"
            },
            engagementStatus: { S: engagementStatuses.contactMade },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }
          },
        },
      },
      {
        PutRequest: {
          // sales rep 1 Needs Defined
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: '1' },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: new Date(2019, 06, 18).getTime().toString() },
            salesRepName: { S: "Jane Doe" },
            client: { S: "Accenture" },
            primaryContactPerson: { S: "Dew Dilbey" },
            title: { S: "C Level" },
            vertical: { S: "Senior Management" },
            meetingLocation: { S: meetingLocation.gotoMeeting },
            purposeOfInteraction: { S: "Negotiations" },
            outcomeAndRemarks: {
              S:
                "He needs to get final approval from the board. Will happen by today EOD"
            },
            engagementStatus: { S: engagementStatuses.needsDefined },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }
          },
        },
      },
      {
        PutRequest: {
          // sales rep 1 Proposal Made
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: '1' },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: new Date(2019, 06, 20).getTime().toString() },
            salesRepName: { S: "Jane Doe" },
            client: { S: "Accenture" },
            primaryContactPerson: { S: "Dew Dilbey" },
            title: { S: "C Level" },
            vertical: { S: "Senior Management" },
            meetingLocation: { S: meetingLocation.gotoMeeting },
            purposeOfInteraction: { S: "Negotiations" },
            outcomeAndRemarks: {
              S:
                "He needs to get final approval from the board. Will happen by today EOD"
            },
            engagementStatus: { S: engagementStatuses.proposalMade },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }
          },
        },
      },
      {
        PutRequest: {
          // sales rep 1 Negotiations Started
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: '1' },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: new Date(2019, 06, 21).getTime().toString() },
            salesRepName: { S: "Jane Doe" },
            client: { S: "Accenture" },
            primaryContactPerson: { S: "Dew Dilbey" },
            title: { S: "C Level" },
            vertical: { S: "Senior Management" },
            meetingLocation: { S: meetingLocation.call },
            purposeOfInteraction: { S: "Negotiations" },
            outcomeAndRemarks: {
              S:
                "He needs to get final approval from the board. Will happen by today EOD"
            },
            engagementStatus: { S: engagementStatuses.negotiationsStarted },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }
          },
        },
      },
      {
        PutRequest: {
          // sales rep 1 Won
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: '1' },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: new Date(2019, 06, 22).getTime().toString() },
            salesRepName: { S: "Jane Doe" },
            client: { S: "Accenture" },
            primaryContactPerson: { S: "Dew Dilbey" },
            title: { S: "C Level" },
            vertical: { S: "Senior Management" },
            meetingLocation: { S: meetingLocation.email },
            purposeOfInteraction: { S: "Negotiations" },
            outcomeAndRemarks: {
              S:
                "He needs to get final approval from the board. Will happen by today EOD"
            },
            engagementStatus: { S: engagementStatuses.won },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) },
            won: { N: '7000' }
          },
        },
      },
      {
        PutRequest: {
          // sales rep 2 Lead In
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "2" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(start_date, 2)) },
            // EVENT_DATE: { N: new Date(2019, 06, 14).getTime().toString() },
            salesRepName: { S: "Tom Roberts" },
            client: { S: "Microsoft" },
            primaryContactPerson: { S: "Danye Halms" },
            title: { S: "VP Level" },
            vertical: { S: "Senior Management" },
            purposeOfInteraction: {
              S: "Confirm final meeting with CEO for pricing  negotiation"
            },
            outcomeAndRemarks: {
              S: "Meeting confirmed for tomorrow"
            },
            engagementStatus: { S: engagementStatuses.leadIn },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }
          }
        },
      },
      {
        PutRequest: {
          // sales rep 2 Contact Made
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "2" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: new Date(2019, 06, 15).getTime().toString() },
            salesRepName: { S: "Tom Roberts" },
            client: { S: "Microsoft" },
            primaryContactPerson: { S: "Danye Halms" },
            title: { S: "VP Level" },
            vertical: { S: "Senior Management" },
            meetingLocation: { S: meetingLocation.call },
            purposeOfInteraction: {
              S: "Confirm final meeting with CEO for pricing  negotiation"
            },
            outcomeAndRemarks: {
              S: "Meeting confirmed for tomorrow"
            },
            engagementStatus: { S: engagementStatuses.contactMade },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        },
      },
      {
        PutRequest: {
          // sales rep 2 Needs Defined
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "2" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: new Date(2019, 06, 17).getTime().toString() },
            salesRepName: { S: "Tom Roberts" },
            client: { S: "Microsoft" },
            primaryContactPerson: { S: "Danye Halms" },
            title: { S: "VP Level" },
            vertical: { S: "Senior Management" },
            meetingLocation: { S: meetingLocation.call },
            purposeOfInteraction: {
              S: "Confirm final meeting with CEO for pricing  negotiation"
            },
            outcomeAndRemarks: {
              S: "Meeting confirmed for tomorrow"
            },
            engagementStatus: { S: engagementStatuses.needsDefined },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }
          }
        },
      },
      {
        PutRequest: {
          // sales rep 2 Proposal Made
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "2" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: new Date(2019, 06, 17).getTime().toString() },
            salesRepName: { S: "Tom Roberts" },
            client: { S: "Microsoft" },
            primaryContactPerson: { S: "Danye Halms" },
            title: { S: "VP Level" },
            vertical: { S: "Senior Management" },
            meetingLocation: { S: meetingLocation.call },
            purposeOfInteraction: {
              S: "Confirm final meeting with CEO for pricing  negotiation"
            },
            outcomeAndRemarks: {
              S: "Meeting confirmed for tomorrow"
            },
            engagementStatus: { S: engagementStatuses.proposalMade },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }
          }
        },
      },
      {
        PutRequest: {
          // sales rep 2 Negotiations Started
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "2" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: new Date(2019, 06, 18).getTime().toString() },
            salesRepName: { S: "Tom Roberts" },
            client: { S: "Microsoft" },
            primaryContactPerson: { S: "Danye Halms" },
            title: { S: "VP Level" },
            vertical: { S: "Senior Management" },
            meetingLocation: { S: meetingLocation.faceToFace },
            purposeOfInteraction: {
              S: "Confirm final meeting with CEO for pricing  negotiation"
            },
            outcomeAndRemarks: {
              S: "Meeting confirmed for tomorrow"
            },
            engagementStatus: { S: engagementStatuses.negotiationsStarted },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        },
      },
      {
        PutRequest: {
          // sales rep 2 Lost
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "2" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: new Date(2019, 06, 18).getTime().toString() },
            salesRepName: { S: "Tom Roberts" },
            client: { S: "Microsoft" },
            primaryContactPerson: { S: "Danye Halms" },
            title: { S: "VP Level" },
            vertical: { S: "Senior Management" },
            meetingLocation: { S: meetingLocation.faceToFace },
            purposeOfInteraction: {
              S: "Confirm final meeting with CEO for pricing  negotiation"
            },
            outcomeAndRemarks: {
              S: "Meeting confirmed for tomorrow"
            },
            engagementStatus: { S: engagementStatuses.lost },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        },
      },
      {
        PutRequest: {
          // sales rep 3 Lead IN
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "3" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(start_date, 2)) },
            // EVENT_DATE: { N: lastWeek.getTime().toString() },
            salesRepName: { S: "Ethan Hunt" },
            client: { S: "Taj Group Of Hotels" },
            primaryContactPerson: { S: "Avie Peasgood" },
            title: { S: "C Level" },
            vertical: { S: "Sales" },
            purposeOfInteraction: { S: "Demo the product" },
            outcomeAndRemarks: {
              S: "She will get back with availablity for the next meeting"
            },
            engagementStatus: { S: engagementStatuses.leadIn },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        },
      },
      {
        PutRequest: {
          // sales rep 3 Contact Made
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "3" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: lastWeek.getTime().toString() },
            salesRepName: { S: "Ethan Hunt" },
            client: { S: "Taj Group Of Hotels" },
            primaryContactPerson: { S: "Avie Peasgood" },
            title: { S: "C Level" },
            vertical: { S: "Sales" },
            meetingLocation: { S: meetingLocation.email },
            purposeOfInteraction: { S: "Demo the product" },
            outcomeAndRemarks: {
              S: "She will get back with availablity for the next meeting"
            },
            engagementStatus: { S: engagementStatuses.contactMade },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        },
      },
      {
        PutRequest: {
          // sales rep 3 Needs Defined
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "3" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: lastWeek.getTime().toString() },
            salesRepName: { S: "Ethan Hunt" },
            client: { S: "Taj Group Of Hotels" },
            primaryContactPerson: { S: "Avie Peasgood" },
            title: { S: "C Level" },
            vertical: { S: "Sales" },
            meetingLocation: { S: meetingLocation.video },
            purposeOfInteraction: { S: "Demo the product" },
            outcomeAndRemarks: {
              S: "She will get back with availablity for the next meeting"
            },
            engagementStatus: { S: engagementStatuses.needsDefined },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        },
      },
      {
        PutRequest: {
          // sales rep 3 Proposal Made
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "3" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: lastWeek.getTime().toString() },
            salesRepName: { S: "Ethan Hunt" },
            client: { S: "Taj Group Of Hotels" },
            primaryContactPerson: { S: "Avie Peasgood" },
            title: { S: "C Level" },
            vertical: { S: "Sales" },
            meetingLocation: { S: meetingLocation.video },
            purposeOfInteraction: { S: "Demo the product" },
            outcomeAndRemarks: {
              S: "She will get back with availablity for the next meeting"
            },
            engagementStatus: { S: engagementStatuses.proposalMade },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        },
      },
      {
        PutRequest: {
          // sales rep 3 Negotiations Started
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "3" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: lastWeek.getTime().toString() },
            salesRepName: { S: "Ethan Hunt" },
            client: { S: "Taj Group Of Hotels" },
            primaryContactPerson: { S: "Avie Peasgood" },
            title: { S: "C Level" },
            vertical: { S: "Sales" },
            meetingLocation: { S: meetingLocation.faceToFace },
            purposeOfInteraction: { S: "Demo the product" },
            outcomeAndRemarks: {
              S: "She will get back with availablity for the next meeting"
            },
            engagementStatus: { S: engagementStatuses.negotiationsStarted },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        },
      },
      {
        PutRequest: {
          // sales rep 3 won
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "3" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            // EVENT_DATE: { N: lastWeek.getTime().toString() },
            salesRepName: { S: "Ethan Hunt" },
            client: { S: "Taj Group Of Hotels" },
            primaryContactPerson: { S: "Avie Peasgood" },
            title: { S: "C Level" },
            vertical: { S: "Sales" },
            meetingLocation: { S: meetingLocation.email },
            purposeOfInteraction: { S: "Demo the product" },
            outcomeAndRemarks: {
              S: "She will get back with availablity for the next meeting"
            },
            engagementStatus: { S: engagementStatuses.won },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) },
            won: { N: '7000' }
          }
        },
      },
      {
        PutRequest: {
          // sales rep 4 Lead In
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "4" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(start_date, 2)) },
            salesRepName: { S: "John Doe" },
            client: { S: "Oberoi Group Of Hotels" },
            primaryContactPerson: { S: "Mahumad Ritelli" },
            title: { S: "C Level" },
            vertical: { S: "Marketin" },
            meetingLocation: { S: "Goto Meeting" },
            purposeOfInteraction: { S: "Understand their current ecosystem" },
            outcomeAndRemarks: {
              S:
                "Send first cut of proposed solution"
            },
            engagementStatus: { S: engagementStatuses.leadIn },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        },
      },
      {
        PutRequest: {
          // sales rep 4 Contact made
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "4" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            salesRepName: { S: "John Doe" },
            client: { S: "Oberoi Group Of Hotels" },
            primaryContactPerson: { S: "Mahumad Ritelli" },
            title: { S: "C Level" },
            vertical: { S: "Marketin" },
            meetingLocation: { S: meetingLocation.call },
            purposeOfInteraction: { S: "Understand their current ecosystem" },
            outcomeAndRemarks: {
              S:
                "Send first cut of proposed solution"
            },
            engagementStatus: { S: engagementStatuses.contactMade },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        },
      },
      {
        PutRequest: {
          // sales rep 4 Needs Defined
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "4" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            salesRepName: { S: "John Doe" },
            client: { S: "Oberoi Group Of Hotels" },
            primaryContactPerson: { S: "Mahumad Ritelli" },
            title: { S: "C Level" },
            vertical: { S: "Marketin" },
            meetingLocation: { S: meetingLocation.gotoMeeting },
            purposeOfInteraction: { S: "Understand their current ecosystem" },
            outcomeAndRemarks: {
              S:
                "Send first cut of proposed solution"
            },
            engagementStatus: { S: engagementStatuses.needsDefined },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        }
      },
      {
        PutRequest: {
          // sales rep 4 Proposal Made
          Item: {
            ID: { S: uuidv4() },
            SALES_REP_ID: { S: "4" },
            EVENT_DATE: { N: toTimestamp(event_date = getEventDate(event_date, 2)) },
            salesRepName: { S: "John Doe" },
            client: { S: "Oberoi Group Of Hotels" },
            primaryContactPerson: { S: "Mahumad Ritelli" },
            title: { S: "C Level" },
            vertical: { S: "Marketin" },
            meetingLocation: { S: meetingLocation.email },
            purposeOfInteraction: { S: "Understand their current ecosystem" },
            outcomeAndRemarks: {
              S:
                "Send first cut of proposed solution"
            },
            engagementStatus: { S: engagementStatuses.proposalMade },
            ALL: { S: 'ALL' },
            date: { S: formatedDate(event_date) }

          }
        }
      }
    ]
  }
};

ddb.batchWriteItem(params, function (err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success", data);
  }
});
