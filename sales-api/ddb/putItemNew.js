const uuidv4 = require("uuid/v4");
// Load the AWS SDK for Node.js
var AWS = require("aws-sdk");
// Set the region
AWS.config.update({ region: "us-east-2" });

// Create the DynamoDB service object
var ddb = new AWS.DynamoDB({ apiVersion: "2012-08-10" });

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
var today = new Date();
var thisWeek = new Date(
  new Date(today).setDate(today.getDate() - getRandomInt(0, 7))
);
var lastWeek = new Date(
  new Date(today).setDate(today.getDate() - getRandomInt(7, 14))
);
var lastMonth = new Date(
  new Date(today).setDate(today.getDate() - getRandomInt(7, 30))
);

var params = {
  RequestItems: {
    SALES_EVENT: [
      {
        PutRequest: {
          Item: {
            YEAR_MONTH: { S: "2019-01" },
            SK: { S: "1#LeadIn" },
            MRR: { N: 3000 },
            EVENT_DATE: { N: today.getTime().toString() },
            salesRepName: { S: "Jane Doe" },
            engagementStatus: { S: "Negotiations Started" },
          }
        }
      },
      {
        PutRequest: {
          Item: {
            SALES_REP_ID: { S: "2" },
            EVENT_DATE: { N: lastMonth.getTime().toString() },
            salesRepName: { S: "Tom Roberts" },
            client: { S: "Microsoft" },
            primaryContactPerson: { S: "Danye Halms" },
            title: { S: "VP Level" },
            vertical: { S: "Senior Management" },
            meetingLocation: { S: "Email" },
            purposeOfInteraction: {
              S: "Confirm final meeting with CEO for pricing  negotiation"
            },
            outcomeAndRemarks: {
              S: "Meeting confirmed for tomorrow"
            },
            engagementStatus: { S: "Negotiations Started" },
            ALL: { S: 'ALL2019' }
          }
        }
      },
      {
        PutRequest: {
          Item: {
            SALES_REP_ID: { S: "3" },
            EVENT_DATE: { N: lastWeek.getTime().toString() },
            salesRepName: { S: "Ethan Hunt" },
            client: { S: "Taj Group Of Hotels" },
            primaryContactPerson: { S: "Avie Peasgood" },
            title: { S: "C Level" },
            vertical: { S: "Sales" },
            meetingLocation: { S: "Video" },
            purposeOfInteraction: { S: "Demo the product" },
            outcomeAndRemarks: {
              S: "She will get back with availablity for the next meeting"
            },
            engagementStatus: { S: "Contact Made" },
            ALL: { S: 'ALL2019' }
          }
        }
      },
      {
        PutRequest: {
          Item: {
            SALES_REP_ID: { S: "4" },
            EVENT_DATE: { N: today.getTime().toString() },
            salesRepName: { S: "John Doe" },
            client: { S: "Oberoi Group Of Hotels" },
            primaryContactPerson: { S: "Mahumad Ritelli" },
            title: { S: "C Level" },
            vertical: { S: "Marketin" },
            meetingLocation: { S: "Goto Meeting" },
            purposeOfInteraction: { S: "Understand their current ecosystem" },
            outcomeAndRemarks: {
              S:
                "Send first cut of proposed solution"
            },
            engagementStatus: { S: "Needs Defined" },
            ALL: { S: 'ALL2019' }
          }
        }
      },
    ]
  }
};

ddb.batchWriteItem(params, function (err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success", data);
  }
});
