const engagementStatusList = [
    "Lead In",
    "Contact Made",
    "Needs Defined",
    "Proposal Made",
    "Negotiations Started",
    "Won"
]

const engagementStatuses = {

    leadIn: "Lead In",
    contactMade: "Contact Made",
    needsDefined: "Needs Defined",
    proposalMade: "Proposal Made",
    negotiationsStarted: "Negotiations Started",
    won: "Won"
}

const meetingLocation = {
    gotoMeeting: "Goto Meeting",
    call: "call",
    faceToFace: "Face to Face",
    video: "Video",
    email: "Email",
    text: "Text"
}

const getStatus = (status) => status.join(" ", '')

const initialStats = _ => ({
    "Lead In": 0,
    "Contact Made": 0,
    "Needs Defined": 0,
    "Proposal Made": 0,
    "Negotiations Started": 0,
    "Won": 0,
    "Lost": 0,
})

const getDateRange = (period, today) => {
    let range = ""
    let todayOnlyDate = new Date(today.getFullYear(), today.getMonth(), today.getDate())
    switch (period) {
        case "Last Month":
            let from = new Date(todayOnlyDate)
            from.setMonth(from.getMonth() - 1)
            from.setDate(1)
            range = {
                from: from.getTime(),
                to: todayOnlyDate.setDate(1)
            }
            break;
        case "Last Week":
            let date = new Date(today);
            let day = date.getDay();
            let prevMonday;
            if (date.getDay() == 0) {
                prevMonday = new Date().setDate(date.getDate() - 7);
            }
            else {
                prevMonday = new Date().setDate(date.getDate() - day - 7);
            }
            let _to = new Date(prevMonday)
            let to = _to.setDate(_to.getDate() + 7)
            range = {
                from: prevMonday,
                to: to
            }
            break;
        default:
            range = {
                from: todayOnlyDate.getTime(),
                to: today.getTime()
            }
    }
    range.from -= 100
    range.to += 100
    console.log(new Date(range.from), new Date(range.to))

    return range
}

module.exports = {
    engagementStatusList,
    engagementStatuses,
    initialStats,
    getDateRange,
    getStatus,
    meetingLocation
}